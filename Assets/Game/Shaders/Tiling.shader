// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Sprites/Default Tiled" {
    Properties {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
            "IgnoreProjector"="False"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }
       
        Pass
        {      
            Cull off
           
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
               
                #include "UnityCG.cginc"
       
                struct appdata {
                    float4 vertex : POSITION;
                    float2 texcoord : TEXCOORD;
                    float4 color: COLOR;
                };
               
                struct v2f {
                    float4 pos : SV_POSITION;
                    float2 uv : TEXCOORD;
                    float4 color: COLOR;
                };
               
                uniform sampler2D _MainTex;
               
                v2f vert (appdata v)  
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv = v.texcoord;
                    o.color = v.color;
                   
                    return o;
                }

                uniform float _ScaleX;
                uniform float _ScaleY;

                half4 frag(v2f i) : COLOR
                {          
                    half4 c = tex2D(_MainTex, fmod(i.uv*float2(_ScaleX,_ScaleY),1)) * i.color;                 
                    return c;
                }   
            ENDCG
        }
    }
    FallBack "Sprites/Default"
}
