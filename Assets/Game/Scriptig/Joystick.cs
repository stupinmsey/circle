﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

class Joystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image joystickImg, backgroundImg, screen;
    private Vector2 inputVector;

    private bool isLeft;
    private bool isRight;

    

    void Awake()
    {
        backgroundImg = transform.GetChild(0).GetComponent<Image>();
        joystickImg = transform.GetChild(0).transform.GetChild(0).GetComponent<Image>();
        screen = transform.GetComponent<Image>();
        backgroundImg.color = joystickImg.color = new Color(1f, 1f, 1f, 0f);
        joystickImg.rectTransform.anchoredPosition = backgroundImg.rectTransform.anchoredPosition = Vector2.zero;
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector2.zero;
        joystickImg.rectTransform.anchoredPosition = backgroundImg.rectTransform.anchoredPosition = inputVector;
        isLeft = isRight =false;
        backgroundImg.color = joystickImg.color = new Color(1f, 1f, 1f, 0f);
        
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        Vector2 click;
        backgroundImg.color = joystickImg.color = new Color(1f, 1f, 1f, 0.25f);
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(screen.rectTransform,
                                                           ped.position,
                                                           ped.pressEventCamera,
                                                           out click))
        {
            backgroundImg.rectTransform.anchoredPosition = new Vector2(click.x, click.y);
        }
        OnDrag(ped);
    }

    public virtual void OnDrag(PointerEventData ped)
    {

        Vector2 pos;
        isLeft = isRight =false;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(backgroundImg.rectTransform,
                                                                   ped.position,
                                                                   ped.pressEventCamera,
                                                                   out pos ))
        {

            pos.x = (pos.x / backgroundImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / backgroundImg.rectTransform.sizeDelta.y);

            if (pos.x > 0) isRight = true;
            else
                if (pos.x < 0) isLeft =true ;

            inputVector = new Vector2(pos.x*2, pos.y*2);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;
            joystickImg.rectTransform.anchoredPosition =
            new Vector3(inputVector.x * (backgroundImg.rectTransform.sizeDelta.x / 3),
                        0); //inputVector.y * (backgroundImg.rectTransform.sizeDelta.y / 3) для джойста вверх-вниз
        }
    }



    public float power = 25;
    private Transform player;
    private Rigidbody2D rb;
    private Text txt;




    void Start()
    {
        player = GameObject.Find("Player").transform;
        rb = player.transform.GetComponent<Rigidbody2D>();
        txt = transform.Find("FPS").GetComponent<Text>();

        if (GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel-1].lvlType == 't')
        {
            GameObject.Find("TimerMenu").GetComponent<Text>().text = " Time: 00.00";
        }
        else GameObject.Find("TimerMenu").GetComponent<Text>().text = null;
    }


    void Update()
    {
        if (Time.timeScale > 0)
        {
            if (fpsBuffer == null || fpsBuffer.Length != frameRange)
            {
                InitializeBuffer();
            }
            UpdateBuffer();
            CalculateFPS();
            txt.text = string.Format("FPS: {0}", AverageFPS);

            if (rb.velocity.x > 7 || rb.velocity.x < -7) rb.velocity = new Vector2(rb.velocity.x * 0.99f, rb.velocity.y);


            if (Input.GetKey(KeyCode.A) || isLeft)
            {

                rb.AddForce(Vector2.left * power);
            }
            else if (Input.GetKey(KeyCode.D) || isRight)
            {

                rb.AddForce(Vector2.left * -power);
            }
        }
    }

    public int frameRange = 60;
    public int AverageFPS { get; private set; }
    int[] fpsBuffer;
    int fpsBufferIndex;

    void InitializeBuffer()
    {
        if (frameRange <= 0)
        {
            frameRange = 1;
        }
        fpsBuffer = new int[frameRange];
        fpsBufferIndex = 0;
    }

    void UpdateBuffer()
    {
        fpsBuffer[fpsBufferIndex++] = (int)(1f / Time.unscaledDeltaTime);
        if (fpsBufferIndex >= frameRange)
        {
            fpsBufferIndex = 0;
        }
    }

    void CalculateFPS()
    {
        int sum = 0;
        for (int i = 0; i < frameRange; i++)
        {
            sum += fpsBuffer[i];
        }
        AverageFPS = (int)((float)sum / frameRange);
    }
}
