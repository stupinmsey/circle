﻿using UnityEngine;
using System.Collections;

public class DialogSystem : MonoBehaviour {


    private string Dialog;
    public GUIStyle guiStyle = new GUIStyle();
    private bool isActive;
    Transform where;
	

    public void Show(string what, float time, Transform where)
    {
        Dialog = what;
        this.where = where;
        isActive = true;
        StartCoroutine(TurnOff(time));
    }

    private IEnumerator TurnOff(float time)
    {
        yield return new WaitForSeconds(time);
        isActive = false;     
    }




	void OnGUI () {
        if (Time.timeScale > 0 && isActive)
        {
            Vector2 pos = Camera.main.WorldToScreenPoint(new Vector2(where.position.x - 3.6f, where.position.y + 3.5f));
            GUI.Label(new Rect(pos.x, -pos.y + Screen.height, 400, 100), Dialog, guiStyle);
            GUI.Box(new Rect(pos.x, -pos.y + Screen.height, 400, 45), "");
        }
	}
}
