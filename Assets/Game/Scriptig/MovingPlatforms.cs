﻿using UnityEngine;
using System.Collections;

public class MovingPlatforms : MonoBehaviour {

    public Transform[] points;

    public bool isMovable;

    public bool isLimitedActions;
    public bool isRotating;
    public bool isRightRotation;
    public bool isChangingRotation;
    public bool isPausingRotation;

    [Range(0.1f, 5)]
    public float timeToChangeRotation;

    [Range(0.1f, 7)]
    public float MoveSpeed;


    [Range(0.1f, 40)]
    public float RotateSpeed;

    [Range(0, 5)]
    public float stopTime;
    public bool isLooping;

    public byte rotateActions;
    public byte moveActions;



    private sbyte curPoint, moveaccess, rotateaccess;
    private bool canMove;
    private bool canRotate;
    private Rigidbody2D rb;
    private float timeRot;

	// Use this for initialization
	void Start () {
        curPoint = 1;
        moveaccess = 1;      
        rotateaccess = 1;
        canMove = canRotate = true;  
        timeRot = timeToChangeRotation;

        rb = (transform.GetComponent<Rigidbody2D>())
            ? transform.GetComponent<Rigidbody2D>()
            : transform.gameObject.AddComponent<Rigidbody2D>(); 

        rb.interpolation = RigidbodyInterpolation2D.Interpolate;
        if (!isMovable) rb.isKinematic = true;
        if (points.Length >= 1) transform.position = points[0].position;
        if (isRightRotation) rotateaccess = -1;
        if (!isLimitedActions) rotateActions = moveActions = 0;
	}
	
	
    void Update()
    {

        if ((canMove && points.Length >= 1) && ((isLimitedActions && moveActions > 0) || !isLimitedActions))
        {
            if (transform.position == points[curPoint].position)
            {
                StartCoroutine(Pause(stopTime,1));
                canMove = false;
                return;
            }
            transform.position = Vector2.MoveTowards(transform.position, points[curPoint].position, Mathf.Sin(2*Mathf.PI * MoveSpeed * Time.deltaTime));
        }

        if ((isRotating && canRotate) && ((isLimitedActions && rotateActions > 0) || !isLimitedActions))
        {
            if (isChangingRotation)
            {
                timeRot -= Time.deltaTime;
                if (timeRot <= 0)
                {
                    if (isPausingRotation)
                    {
                        canRotate = false;
                        StartCoroutine(Pause(stopTime, 2));
                    }
                    else StartCoroutine(Pause(0, 2));

                    timeRot = timeToChangeRotation;
                }
            }

            transform.Rotate(0, 0, 30 * rotateaccess * RotateSpeed * Time.deltaTime);                
        }

    }




    private IEnumerator Pause(float time, sbyte mode )
    {
        switch (mode)
        {
            case 1:
                {
                    if ((curPoint == 0 && moveaccess == -1) || (curPoint == points.Length - 1 && moveaccess == 1))
                    {
                        moveaccess *= -1;
                        if (isLooping && curPoint == points.Length - 1) { curPoint = -1; moveaccess = 1; }
                    }
                    curPoint += moveaccess;
                    yield return new WaitForSeconds(time);
                    canMove = true;
                }
                if (moveActions > 0) moveActions--;
                break;

            case 2:
                {
                    if(isChangingRotation) rotateaccess *= -1;
                    yield return new WaitForSeconds(time);
                    canRotate = true;
                }
                if (rotateActions > 0) rotateActions--;
                break;
        }



    }

    void OnDrawGizmos()
    {
            for (int i = 0; i <= points.Length - 2; i++)
            {
                Gizmos.DrawLine(points[i].position, points[i + 1].position);
            }

    }
}
