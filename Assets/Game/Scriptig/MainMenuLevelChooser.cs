﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuLevelChooser : MonoBehaviour, IPointerDownHandler
{
    [Header("Здесь воткнуть все уровни на карте.")]
    public Transform[] Container;
    private Transform Data;
    private SaveLoad data;
    private Camera cam;

  
    public Transform infoPanel;

    

    void Start()
    {
        Data = GameObject.Find("Data.bin").transform;
        data = Data.GetComponent<SaveLoad>();


        for (int i = 1; i <= data.level.Length - 1; i++)
        {
            if (data.level[i].isUnlocked)
            {
                Container[i].transform.Find("Status").
                transform.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            }
            if (data.level[i].score == 1)
            {
                Container[i].transform.Find("Star_1").
                transform.GetComponent<SpriteRenderer>().color = Color.yellow;
            }
            if (data.level[i].score == 2)
            {
                Container[i].transform.Find("Star_1").
                transform.GetComponent<SpriteRenderer>().color = Color.yellow;
                Container[i].transform.Find("Star_2").
                transform.GetComponent<SpriteRenderer>().color = Color.yellow;
            }
            if (data.level[i].score == 3)
            {
                Container[i].transform.Find("Star_3").
                transform.GetComponent<SpriteRenderer>().color = Color.yellow;
                Container[i].transform.Find("Star_2").
                transform.GetComponent<SpriteRenderer>().color = Color.yellow;
                Container[i].transform.Find("Star_1").
                transform.GetComponent<SpriteRenderer>().color = Color.yellow;

            }
        }

        infoPanel.gameObject.SetActive(false);

    }


    public virtual void OnPointerDown(PointerEventData ped)
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(ped.position), Vector2.zero);
        if (hit.collider != null)
        {
            int index = 0;

            if (hit.collider.name.Substring(0, 16) == "LEVEL_CONTAINER_")
            {
                if (hit.collider.name.Length > 17) index = int.Parse(hit.collider.name.Substring(16, 2));//выдираем номер контейнера из строки
                else index = int.Parse(hit.collider.name.Substring(16, 1));
                Debug.Log(index+1);
                infoPanel.gameObject.SetActive(true);

                //info level
                infoPanel.GetComponent<SwitchLevelMainMenu>().SetStatusLevel
                    (
                    index,
                    data.level[index].isUnlocked,
                    data.level[index].lvlType,
                    data.level[index].fixedparameter,
                    data.level[index].beatenparameter                    
                    );


                if (data.level[index].isUnlocked) infoPanel.GetComponent<SwitchLevelMainMenu>().SetLevel(index);
                else infoPanel.GetComponent<SwitchLevelMainMenu>().SetLevel(-1);
            }
        }
        else infoPanel.gameObject.SetActive(false);
    }
}

