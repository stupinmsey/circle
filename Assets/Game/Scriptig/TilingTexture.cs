﻿using UnityEngine;
using System.Collections;

 [RequireComponent(typeof(SpriteRenderer))]
public class TilingTexture : MonoBehaviour {

     public Shader shader;
     private Texture2D Texture;
     private SpriteRenderer sr;

     void Start()
     {
         sr = transform.GetComponent<SpriteRenderer>();
         Texture = sr.sprite.texture;
         Material mat = new Material(shader);
         mat.SetTexture("_MainTex", Texture);
     }

     void Update()
     {
             Material mat = new Material(shader);
             mat.SetTexture("_MainTex", Texture);
             mat.SetFloat("_ScaleX", transform.localScale.x);
             mat.SetFloat("_ScaleY", transform.localScale.y);
             sr.material = mat;
         
     }    
}

