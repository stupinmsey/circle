﻿using UnityEngine;
using System.Collections;

public class ButtonTrigger : MonoBehaviour
{

    public float pauseTime = 0;
    public Transform button;
    public Transform subject;


    public bool t_MovingPlatform;
    public bool t_ChangeTrigger;


    private Vector2 bPos, fPos;
    private bool moveDown, moveUp, isClicked;
    private byte count;


    private Color c1, c2;

    private bool colorFrom_A_To_B;
    private bool changeColor;


    void Start()
    {
        
        bPos = button.position;
        fPos = button.parent.position;
        moveUp = moveDown = changeColor=false;
        count = 0;


        c1 = subject.GetComponent<SpriteRenderer>().color;
        c2 = new Color32(70,255,140,255); 
    }

    void Update()
    {


        if (moveDown)
        {
            if (button.localPosition.y <= 0.25) 
            { 
                moveDown = false;
                subject.GetComponent<PolygonCollider2D>().isTrigger = false;
                subject.gameObject.layer = 8;
                return; 
            }
            button.position = Vector2.Lerp(button.position, fPos, 5 * Time.deltaTime);
        }
        else
            if (moveUp)
            {
                if (button.localPosition.y >= 1) 
                {
                    moveUp = false;
                    subject.GetComponent<PolygonCollider2D>().isTrigger = true;
                    subject.gameObject.layer = 0;
                    return; 
                }
                if (count > 0) { moveUp = false; moveDown = true; return; }
                button.position = Vector2.Lerp(button.position, bPos, 5 * Time.deltaTime);
            }
    }


    void FixedUpdate()
    {
        if (t_ChangeTrigger && changeColor)
        {

            if (colorFrom_A_To_B)
            {
                subject.GetComponent<SpriteRenderer>().color = Color.Lerp(subject.GetComponent<SpriteRenderer>().color, c1, Time.time / 100);
            }
            else
            {
                subject.GetComponent<SpriteRenderer>().color = Color.Lerp(subject.GetComponent<SpriteRenderer>().color, c2, Time.time / 100);
            }
        }
    }

    private IEnumerator UnClick(float time)
    {

        yield return new WaitForSeconds(time);
        if (count == 0 && !moveDown)
        {
            moveUp = true;
            if (t_ChangeTrigger)
            {
                colorFrom_A_To_B = true;
                changeColor = true;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (t_ChangeTrigger) { colorFrom_A_To_B = false; changeColor = true; }
        moveDown = true;
        moveUp = false;
        count++;
    }

    void OnTriggerStay2D(Collider2D c)
    {
        if (t_MovingPlatform)
        {
            if (subject.GetComponent<MovingPlatforms>().moveActions == 0)
            subject.GetComponent<MovingPlatforms>().moveActions = 2;


            if (subject.GetComponent<MovingPlatforms>().rotateActions == 0)
            subject.GetComponent<MovingPlatforms>().rotateActions = 2;
        }
    }




    void OnTriggerExit2D(Collider2D c)
    {
        if (t_MovingPlatform)
        {
            if (subject.GetComponent<MovingPlatforms>().moveActions == 1)
                subject.GetComponent<MovingPlatforms>().moveActions = 3;
            else
                if (subject.GetComponent<MovingPlatforms>().moveActions == 0)
                    subject.GetComponent<MovingPlatforms>().moveActions = 2;

            if (subject.GetComponent<MovingPlatforms>().rotateActions == 1)
                subject.GetComponent<MovingPlatforms>().rotateActions = 3;
            else
                if (subject.GetComponent<MovingPlatforms>().rotateActions == 0)
                    subject.GetComponent<MovingPlatforms>().rotateActions = 2;
        }
        moveDown = true;
        moveUp = false;
        count--;
        StartCoroutine(UnClick(pauseTime));
    }



}
