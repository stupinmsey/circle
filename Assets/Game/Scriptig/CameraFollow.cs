﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    [Range(0, 5)]
    public float backgroundSpd;
    public Transform Background;
    public Transform player;
    public bool existBackGround;

    private bool isEnabled;
    private Material mat;



    private float currentSpd, prevSpd;

    void Start()
    {
        if (Background)
        {
            prevSpd = player.transform.position.x;


            mat = Background.GetComponent<Renderer>().material 
                          ? Background.GetComponent<Renderer>().material 
                          : Background.gameObject.AddComponent<Renderer>().material;
        }
        EnableCam();
    }

    void Update()
    {
        if (isEnabled)
        {
            transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, -1),
                                              new Vector3(player.transform.position.x, player.transform.position.y, -1),
                                              Time.deltaTime * 3f);
        

        if (existBackGround)
        {
            currentSpd = player.transform.position.x;

            mat.mainTextureOffset = Vector2.Lerp(new Vector2((backgroundSpd * currentSpd)/40f, 0f), 
                                                    new Vector2((backgroundSpd * prevSpd)/40f, 0f), Time.deltaTime);
            prevSpd = currentSpd;
        }
        }
    }

    public void DisableCam()
    {
        isEnabled = false;
    }
    public void EnableCam()
    {
        isEnabled = true;
    }
}
