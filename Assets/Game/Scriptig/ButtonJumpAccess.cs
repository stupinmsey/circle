﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ButtonJumpAccess : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    public bool isJumpButton, isOptions, isCloseOptions, is_option1, is_option2, is_option3;


    private bool access = false;
    private Transform player;
    private Rigidbody2D rb;
    private float power = 25;
    private float coeff = 1.5f;
    private Vector2 groundCheck;
    private LayerMask ground = 1 << 8;
    private float groundRadius;



    // Use this for initialization
    void Start()
    {
        if (isJumpButton)
        {
            player = GameObject.Find("Player").transform;
            rb = player.GetComponent<Rigidbody2D>();
            groundRadius = player.transform.GetComponent<CircleCollider2D>().radius / 2.7f;
        }
    }

    // Update is called once per frame
    public virtual void OnPointerEnter(PointerEventData ped)
    {
        if (isJumpButton) access = true;
    }


    public virtual void OnPointerDown(PointerEventData ped)
    {
        if (isOptions)
        {
            transform.GetComponent<Image>().enabled = false;
            GameObject.Find("Special_Menu").GetComponent<Image>().enabled =
            GameObject.Find("Button_CloseMenu").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_1").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_2").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_3").GetComponent<Image>().enabled = true;
            WaterLine[] waters = GameObject.FindObjectsOfType<WaterLine>();
            foreach (WaterLine water in waters) water.enabled = false;
            Time.timeScale = 0;
        }
        else if (isCloseOptions)
        {
            transform.GetComponent<Image>().enabled =
            GameObject.Find("Special_Menu").GetComponent<Image>().enabled =
            GameObject.Find("Button_CloseMenu").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_1").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_2").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_3").GetComponent<Image>().enabled = false;
            GameObject.Find("Button_Menu").GetComponent<Image>().enabled = true;
            WaterLine[] waters = GameObject.FindObjectsOfType<WaterLine>();
            foreach (WaterLine water in waters) water.enabled = true;
            Time.timeScale = 1;
        }
        else if (is_option1)
        {
            Time.timeScale = 1;
            Application.LoadLevel(1);
        }
        else if (is_option2)
        {
            Time.timeScale = 1;
            Application.LoadLevel(Application.loadedLevel);
        }
        else if (is_option3)
        {
            GameObject go_1, go_2;
            RectTransform rt_1, rt_2;
            go_1 = GameObject.Find("Panel");
            go_2 = GameObject.Find("Button_Jump");
            rt_1 = go_1.GetComponent<RectTransform>();
            rt_2 = go_2.GetComponent<RectTransform>();
            Vector2 temp = rt_1.localPosition;
            rt_1.localPosition = rt_2.localPosition;
            rt_2.localPosition = temp;
            /// close menu
            transform.GetComponent<Image>().enabled =
            GameObject.Find("Special_Menu").GetComponent<Image>().enabled =
            GameObject.Find("Button_CloseMenu").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_1").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_2").GetComponent<Image>().enabled =
            GameObject.Find("Button_option_3").GetComponent<Image>().enabled = false;
            GameObject.Find("Button_Menu").GetComponent<Image>().enabled = true;
            Time.timeScale = 1;

        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (isJumpButton) access = false;
    }
    void FixedUpdate()
    {
        if (isJumpButton)
        {
            groundCheck = new Vector3(player.transform.position.x, player.transform.position.y - 0.25f);

            if ((Input.GetKey(KeyCode.W) || access) && Physics2D.OverlapCircle(groundCheck, groundRadius, ground))
            {
                rb.velocity = new Vector2(rb.velocity.x, power / coeff);
            }
        }
    }
}


