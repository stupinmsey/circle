﻿using UnityEngine;
using System.Collections.Generic;

public class GL_DrawLines_red : MonoBehaviour
{
    Camera cam;
    Material material2D;
   

    Dictionary<int, Vector2> hash_from;
    Dictionary<int, Vector2> hash_to;
    ICollection<int> hash_keys;

    void Start()
    {        
        hash_from = new Dictionary<int, Vector2>();
        hash_to = new Dictionary<int, Vector2>();
        Shader sh = Shader.Find("GUI/Text Shader");
        material2D = new Material(sh);
        cam = transform.gameObject.GetComponent<Camera>();
    }

    public void SetCoord(int hash_code, Vector2 from, Vector2 to)
    {
        if (hash_from.ContainsKey(hash_code) && hash_to.ContainsKey(hash_code))
        {
            hash_from[hash_code] = cam.WorldToScreenPoint(from);
            hash_to[hash_code] = cam.WorldToScreenPoint(to);
        }
        else
        {
            hash_from.Add(hash_code, from);
            hash_to.Add(hash_code, to);
        }
    }

    void OnPostRender()
    {
            material2D.SetPass(0);
            GL.PushMatrix();
            GL.LoadOrtho();
            GL.Begin(GL.LINES);
            GL.Color(Color.red);
                hash_keys = hash_from.Keys;
                foreach (int key in hash_keys)
                {
                    GL.Vertex(new Vector2(hash_from[key].x / Screen.width, hash_from[key].y / Screen.height));
                    GL.Vertex(new Vector2(hash_to[key].x / Screen.width, hash_to[key].y / Screen.height));
                }   

        GL.End();
        GL.PopMatrix();
    }
}
