﻿using UnityEngine;
using System.Collections;

public class GettingOutObject : MonoBehaviour {


    
    Transform block, player;
 
    float maxdistance, distance;

    

	void Start () 
    {
        maxdistance = transform.GetComponent<CircleCollider2D>().radius;       
        block = transform.GetChild(0);
        player = GameObject.Find("Player").transform;   
	}
	
	// Update is called once per frame
	void Update () 
    {

        distance = Vector2.Distance(new Vector2(transform.position.x, 0), new Vector2(player.position.x, 0));
        if (distance <= maxdistance*3) 
        {
            float resolution =(maxdistance/distance<3.6f)? maxdistance/distance: 3.6f;
            block.localPosition = new Vector2(block.localPosition.x, (resolution));
            
        }                

	}


}
