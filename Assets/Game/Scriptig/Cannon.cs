﻿using UnityEngine;
using System.Collections;

public class Cannon : MonoBehaviour {

	// Use this for initialization
    [Header("Global Settings")]

    [Range(0.5f, 4)]
    public float CannonReaction;
    [Range(0.13f, 4)]
    public float bulletQueueTimer;

    public bool isRotating;

    [Header("Strike type Settings")]
    public bool withLaser;
    public bool withBullets;
    public bool KillingLaser;
    public bool PushingLaser;

    public GameObject spawnBullet;
    private Bullet bulletScript;
    private Transform target;
    private Rigidbody2D targetRB;
    private GL_DrawLines_red line_red;
    private GL_DrawLines_green line_green;
    private Transform Camera;
    private bool noTarget;
    private bool canStrike;

    private float _bulletQueueTimer;

	void Start () 
    {
        target = GameObject.Find("Player").transform;
        targetRB = target.GetComponent<Rigidbody2D>();
        Camera = GameObject.Find("Main Camera").transform;
        line_red = (Camera.GetComponent<GL_DrawLines_red>()) ?
                Camera.GetComponent<GL_DrawLines_red>() :
                Camera.gameObject.AddComponent<GL_DrawLines_red>();

        line_green = (Camera.GetComponent<GL_DrawLines_green>()) ?
        Camera.GetComponent<GL_DrawLines_green>() :
        Camera.gameObject.AddComponent<GL_DrawLines_green>();        
        noTarget = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!noTarget && Vector2.Distance(transform.position, target.position) < 20 && isRotating)
        {
            Vector2 dir = target.position - transform.position;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - 90;

            transform.rotation = Quaternion.Lerp(transform.rotation,
                                                Quaternion.Euler(transform.rotation.x, transform.rotation.y, angle),
                                                Time.deltaTime * CannonReaction);
        }
        else transform.rotation = transform.rotation;





        RaycastHit2D hit = Physics2D.Raycast(transform.GetChild(0).position, transform.up, Mathf.Infinity);


        _bulletQueueTimer += Time.deltaTime;
        if (_bulletQueueTimer >= bulletQueueTimer)
        {
            if (withBullets)
            {
                GameObject obj = Instantiate<GameObject>(spawnBullet);
                obj.transform.position = transform.GetChild(0).position;
                bulletScript = obj.transform.GetComponent<Bullet>() ? obj.transform.GetComponent<Bullet>() : obj.AddComponent<Bullet>();
                bulletScript.OnInit("direct", hit.point, 10);
                _bulletQueueTimer = 0;
            }
        }


        if (hit.collider.name == "Player")
        {
            if (withLaser)
            {
                if (KillingLaser)
                {
                    if (!noTarget)
                    {
                        Kill();
                        noTarget = true;
                    }
                    line_green.SetCoord(transform.GetHashCode(), Vector2.zero, Vector2.zero);
                    line_red.SetCoord(transform.GetHashCode(), transform.GetChild(0).position, hit.point);
                }
                else
                {
                    line_green.SetCoord(transform.GetHashCode(), transform.GetChild(0).position, hit.point);
                    line_red.SetCoord(transform.GetHashCode(), Vector2.zero, Vector2.zero);
                    if (PushingLaser)
                    {
                        targetRB.AddForce((target.position - transform.position).normalized * 15);
                    }
                }
            }           
            
        }
        else
        {
            if (withLaser)
            {
                if (KillingLaser)
                {
                    line_green.SetCoord(transform.GetHashCode(), Vector2.zero, Vector2.zero);
                    line_red.SetCoord(transform.GetHashCode(), transform.GetChild(0).position, hit.point);
                }
                else
                {
                    line_green.SetCoord(transform.GetHashCode(), transform.GetChild(0).position, hit.point);
                    line_red.SetCoord(transform.GetHashCode(), Vector2.zero, Vector2.zero);
                }
            }
        }  
	}

    void Kill()
    {
        GameObject empty = new GameObject();
        ActivatingTrigger trigger = empty.AddComponent<ActivatingTrigger>();
        trigger._Camera = Camera;
        trigger.Die(target);
    }
}
