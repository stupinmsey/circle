﻿using UnityEngine;
using System.Collections;

public class SpriteChanger : MonoBehaviour
{

    private Texture2D originalTexture, tmpTexture;
    private float Timer;
    private int textureWidth, textureHeight;
    private Sprite sprite;

    private BoxCollider2D currentCollider;
    Vector2 standartPivot = new Vector2(0.5f, 0.5f);
    Rect standartRect;
    void Start()
    {

        currentCollider = transform.GetComponent<BoxCollider2D>();
        originalTexture = GetComponent<SpriteRenderer>().sprite.texture;

        textureWidth = originalTexture.width;
        textureHeight = originalTexture.height;
        standartRect = new Rect(0, 0, textureWidth, textureHeight);


        //setting temp texture width and height 
        tmpTexture = new Texture2D(originalTexture.width, originalTexture.height);
        //fill the new texture with the original one (to avoid "empty" pixels)
        for (int y = 0; y < tmpTexture.height; y++)
        {
            for (int x = 0; x < tmpTexture.width; x++)
            {
                tmpTexture.SetPixel(x, y, originalTexture.GetPixel(x, y));
            }
        }
       // ClearTexture(tmpTexture);
    }



    void ClearTexture(Texture2D texture)
    {
        for (int x = 0; x <= texture.width; x++)
        {
            for (int y = 0; y <= texture.height; y++)
            {
                DrawTexture(texture,x, y, new Color32(0, 0, 0, 0));
            }
        }
    }




    void DrawTexture(Texture2D texture, int x, int y, Color32 col)
    {
        texture.SetPixel(x+1, y, col);
        texture.SetPixel(x + 1, y + 1, col);
        texture.SetPixel(x + 1, y - 1, col);
        texture.SetPixel(x-1, y, col);
        texture.SetPixel(x - 1, y + 1, col);
        texture.SetPixel(x - 1, y - 1, col);
        texture.SetPixel(x, y+1, col);
        texture.SetPixel(x, y-1, col);
        texture.SetPixel(x, y, col);
        texture.Apply();
        sprite = Sprite.Create(texture,
        standartRect,
        standartPivot,
        100);
        GetComponent<SpriteRenderer>().sprite = sprite;
    }


    void OnCollisionEnter2D(Collision2D col)
    {
        Test(col);
    }

    void OnCollisionStay2D(Collision2D col)
    {
        Test(col);
    }

    void Test(Collision2D col)
    {

        var variation_x = Mathf.Abs(currentCollider.bounds.max.x - col.contacts[0].point.x);
        var variation_y = Mathf.Abs(currentCollider.bounds.max.y - col.contacts[0].point.y);
        var max_x = Mathf.Abs(currentCollider.bounds.max.x - currentCollider.bounds.min.x);
        var max_y = Mathf.Abs(currentCollider.bounds.max.y - currentCollider.bounds.min.y);

        
        

        if (col.collider.GetComponent<Rigidbody2D>())
        {
            Color32 color = col.collider.transform.GetComponent<SpriteRenderer>().color;
            int paintX = (int)((1 - variation_x / max_x) * (textureWidth / 1 - variation_x));
            int paintY = (int)((1 - variation_y / max_y) * (textureHeight / 1 - variation_y));

            if(paintX>= textureWidth -1)
            {
                if (paintY <= textureHeight - 1 && paintY >= 1)
                {
                    //print("right");
                    DrawTexture(tmpTexture, textureWidth-2, paintY, color);
                }
            }
            else if (paintX <= textureWidth -1 && paintX >= 2)
            {
                if (paintY >= textureHeight - 1)
                {
                    //print("top");
                    DrawTexture(tmpTexture, paintX, textureHeight-2, color);
                }
                else
                {
                    //print("bot");
                    DrawTexture(tmpTexture, paintX, 1, color);
                }
            }
            else if (paintX <= 2)
            {
                if (paintY <= textureHeight - 1 && paintY >= 1)
                {
                    //print("left");
                    DrawTexture(tmpTexture, 1, paintY, color);
                }
            }
            
        }
    }
}
