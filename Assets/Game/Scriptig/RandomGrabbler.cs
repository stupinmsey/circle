﻿using UnityEngine;
using System.Collections;

public class RandomGrabbler : MonoBehaviour 
{
    float MaxBoundTimer;

    float RotateTimer;
    bool Search, Relocate, ForbiddenDraw;
    Vector2 RelocatePoint, FromPoint;

    Transform Camera;
    GL_DrawLines_red line;

    float dist;
    float z;



    void Kill()
    {
        Transform player = GameObject.Find("Player").transform;
        GameObject empty = new GameObject();
        ActivatingTrigger trigger = empty.AddComponent<ActivatingTrigger>();
        trigger._Camera = GameObject.Find("Main Camera").transform;
        trigger.Die(player);
    }

	void Start () 
    {
        Search = true;
        Camera = GameObject.Find("Main Camera").transform;
        line = (Camera.GetComponent<GL_DrawLines_red>()) ?
                Camera.GetComponent<GL_DrawLines_red>() : 
                Camera.gameObject.AddComponent<GL_DrawLines_red>();
        MaxBoundTimer = Random.Range(0.6f, 1.3f);
        FromPoint = transform.position;
	}


	
	void FixedUpdate () {

        RotateTimer += Time.deltaTime;
        if (RotateTimer >= MaxBoundTimer)
        {
            ForbiddenDraw = false;
            z = Random.Range(1, 360);
            transform.eulerAngles = new Vector3(0, 0, z);
            Search = true;
            RotateTimer = 0;
            dist = 0;
        }

        FromPoint = transform.position;

        if (Search)
        {
            dist += Time.deltaTime;
            RelocatePoint = Vector2.MoveTowards(FromPoint, (Vector3)FromPoint + transform.up * 200, dist * 15);
            Collider2D col = Physics2D.OverlapPoint(RelocatePoint);
            if (col)
            {
                if (col.name == "Player") Kill();
                Search = false;
                Relocate = true;
            }
        }

        
        if (Relocate)
        {
            transform.position = Vector2.Lerp(FromPoint, RelocatePoint, Time.deltaTime * 3);
            if (Vector2.Distance(FromPoint, RelocatePoint) <= 0.3) Relocate = false;
        }

        if (!ForbiddenDraw) DrawLine(FromPoint, RelocatePoint, Color.red);
        else DrawLine(Vector2.zero, Vector2.zero, Color.red);
	}

    void DrawLine(Vector2 from, Vector2 to, Color color)
    {
        line.SetCoord(transform.gameObject.GetHashCode(),from, to);
    }


    private void OnTriggerEnter2D(Collider2D who)
    {
        Relocate = Search= false;
        ForbiddenDraw = true;
    }




}
