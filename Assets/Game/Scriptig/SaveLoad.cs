﻿using UnityEngine;
using System;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


[Serializable]
public class LeveL
{
    public int lvlNumber { get; set; }               //номер лвла
    public bool isUnlocked { get; set; }             //открыт лвл?
    public char lvlType { get; private set; }        //тип лвла стары\тайминг\жизни
    public float fixedparameter { get; private set; }//номер лвла
    public float beatenparameter { get; set; }       //номер лвла
    public byte score { get; set; }                  //номер лвла


    public LeveL(int lvlNumber, bool isUnlocked, char lvlType, float fixedparameter, float beatenparameter, byte score)
    {
        this.lvlNumber = lvlNumber;
        this.isUnlocked = isUnlocked;
        this.lvlType = lvlType;
        this.fixedparameter = fixedparameter;
        this.beatenparameter = beatenparameter;
        this.score = score;
    }
    private LeveL(){}
}



class SaveLoad : MonoBehaviour
{


    BinaryFormatter formatter = new BinaryFormatter();
    public LeveL[] level = new LeveL[6];
    FileStream fs;

    void Start()
    {
        DontDestroyOnLoad(transform);
        if (!System.IO.File.Exists(Application.persistentDataPath + "/data.dat"))
        {

            level[1] = new LeveL(1, true, 's', 1, 0, 0);
            level[2] = new LeveL(2, false, 's', 3, 0, 0);
            level[3] = new LeveL(3, false, 't', 30, 0, 0);
            level[4] = new LeveL(4, false, 's', 5, 0, 0);
            level[5] = new LeveL(5, false, 't', 10, 0, 0);

            using (fs = new FileStream(Application.persistentDataPath + "/data.dat", FileMode.Create))
            {
                formatter.Serialize(fs, level);
            }
        }
        else
        {
            using (fs = new FileStream(Application.persistentDataPath + "/data.dat", FileMode.Open))
            {
                level = (LeveL[])formatter.Deserialize(fs);
            }
        }
        Application.LoadLevel(Application.loadedLevel + 1); // Загружаем следующий уровень с сохраненной Data
    }


    public void Save(int levelNum, byte command, float value = 0)
    {
        using (fs = new FileStream(Application.persistentDataPath + "/data.dat", FileMode.Open))
        {
            level = (LeveL[])formatter.Deserialize(fs);
            switch (command)
            {
                case 1: //isUnlocked
                    {
                        if (value > 0) level[levelNum].isUnlocked = true;
                    }
                    break;
                case 2: // setrecord
                    {
                        if (level[levelNum].lvlType == 's')
                        {
                            if (value > level[levelNum].beatenparameter) level[levelNum].beatenparameter = value;
                        }
                        else if (level[levelNum].lvlType == 't')
                        {
                            //if (value == level[levelNum].fixedparameter) return;
                            if (value > level[levelNum].beatenparameter) { level[levelNum].beatenparameter = value; Debug.Log(string.Format("beaten={0}", value)); }
                        }
                    }
                    break;
                case 3: // setstars by record
                    {                        
                        if (level[levelNum].lvlType == 's')
                        {

                                if (value >= level[levelNum].fixedparameter / 1) { level[levelNum].score = 3; break; }
                                else
                                    if (value > level[levelNum].fixedparameter / 2) { level[levelNum].score = 2; break; }
                                    else
                                        if (value >= level[levelNum].fixedparameter / 3) { level[levelNum].score = 1; break; }
                        }
                        else if (level[levelNum].lvlType == 't')
                        {

                            if (level[levelNum].beatenparameter == level[levelNum].fixedparameter) return;
                            Debug.Log(string.Format("70%= {0} , 50%= {1} ", level[levelNum].fixedparameter * 0.75, level[levelNum].fixedparameter * 0.5));
                            if (level[levelNum].beatenparameter > 0)
                            {
                                level[levelNum].score = 1;
                                if (level[levelNum].beatenparameter >= level[levelNum].fixedparameter * 0.5)
                                {
                                    level[levelNum].score = 2;
                                    if (level[levelNum].beatenparameter >= level[levelNum].fixedparameter * 0.75)
                                    {
                                        level[levelNum].score = 3;
                                    }
                                }
                            }
                            
                        }

                    }
                    break;

                case 4:
                    {

                    }
                    break;
            }
        }
        using (fs = new FileStream(Application.persistentDataPath + "/data.dat", FileMode.Create))
        {
            formatter.Serialize(fs, level);
        }

    }
}
