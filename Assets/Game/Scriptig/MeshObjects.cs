﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MeshObjects : MonoBehaviour
{
    public bool isTriangle, isQuad;
    private GameObject BodyMesh;
    private Mesh m_meshTriangle;


    void Start()
    {
        isTriangle = true;
        BodyMesh = transform.gameObject;
        if (!BodyMesh.GetComponent<MeshFilter>()) BodyMesh.AddComponent<MeshFilter>();
        if (!BodyMesh.GetComponent<MeshRenderer>()) BodyMesh.AddComponent<MeshRenderer>();
        if (isTriangle) DrawTriangle();
        else
            if (isQuad) DrawQuad();

    }

    private void DrawTriangle()
    {
        if (Application.isPlaying) m_meshTriangle = BodyMesh.GetComponent<MeshFilter>().sharedMesh;
        else m_meshTriangle = BodyMesh.GetComponent<MeshFilter>().mesh;        

        float x = transform.position.x;
        float y = transform.position.y;

        m_meshTriangle.vertices = new Vector3[]
       {
           new Vector2(-1,0),
           new Vector2(0,2),
           new Vector2(1,0)
       };

        m_meshTriangle.uv = new Vector2[]
       {
           new Vector2(x-1,y),
           new Vector2(x,y+1),
           new Vector2(x+1,y)
       };
        m_meshTriangle.triangles = new int[] { 0, 1, 2 };

        m_meshTriangle.RecalculateBounds();
        m_meshTriangle.RecalculateNormals();
    }

    private void DrawQuad()
    {
                if (Application.isPlaying) m_meshTriangle = BodyMesh.GetComponent<MeshFilter>().sharedMesh;
        else m_meshTriangle = BodyMesh.GetComponent<MeshFilter>().mesh;        

        //float x = transform.position.x;
        //float y = transform.position.y;
    }


   
}