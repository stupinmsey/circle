﻿using UnityEngine;
using System.Collections;

public class Preload : MonoBehaviour {

    private GameObject[] peaces;
    private Vector2 faraway = new Vector2(-500, -500);
    private bool isActiveTimer;
    public int stars;
    public float timer;

    private UnityEngine.UI.Text txt;

    void Start()
    {
        stars = 0;
        peaces = new GameObject[30];
        
        for (int i = 0; i <= peaces.Length - 1; i++)
        {
            peaces[i] = (GameObject)Instantiate(Resources.Load("PlayerPeaces"));
            DontDestroyOnLoad(peaces[i]);
            peaces[i].transform.position = faraway;
            peaces[i].SetActive(false);
        }
    }

    public void HidePieces()
    {
        for (int i = 0; i <= peaces.Length - 1; i++)
        {
            peaces[i].GetComponent<Rigidbody2D>().mass = 0.8f;
            peaces[i].transform.position = faraway;
            peaces[i].SetActive(false);
        }
    }
	
    public void CreatePieces(Vector2 where)
    {

        for (int i = 0; i <= peaces.Length - 1; i++)
        {
            int x = Random.Range(-11, 11);
            int y = Random.Range(-11, 11);
            peaces[i].SetActive(true);
            peaces[i].transform.position = new Vector2(where.x + x / 8, where.y + y / 8);
            peaces[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(x, y), ForceMode2D.Impulse);
            peaces[i].GetComponent<Rigidbody2D>().mass = 0.2f;

        }
    }

    public void StartTimer(float startTime)
    {
        txt = GameObject.Find("TimerMenu").GetComponent<UnityEngine.UI.Text>();
        timer = startTime;
        isActiveTimer = true;
    }

    public void EndTimer()
    {
        timer = 0.00f;
        txt.text = " Time: " + timer.ToString("00.00");
        isActiveTimer = false;
    }


    public void PauseTimer()
    {
        isActiveTimer = false;
    }

    public void UnPauseTimer()
    {
        isActiveTimer = true;
    }

    private void FixedUpdate()
    {        
        if (isActiveTimer)
        {
            if (timer <= 0.0)
            {
                EndTimer();
                Transform player = GameObject.Find("Player").transform;
                GameObject empty = new GameObject();
                ActivatingTrigger trigger=empty.AddComponent<ActivatingTrigger>();
                trigger._Camera = GameObject.Find("Main Camera").transform;
                trigger.Die(player);
                return;
            }
            timer -= Time.deltaTime;  
            txt.text = " Time: "+timer.ToString("00.00");
           
        }
    }
}
