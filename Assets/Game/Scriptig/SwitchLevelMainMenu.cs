﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class SwitchLevelMainMenu : MonoBehaviour, IPointerDownHandler {

    private Image ok, cancel;
    private int index;
	// Use this for initialization
	void Start () {

        ok = transform.Find("OK").transform.GetComponent<Image>();
	}


    public virtual void OnPointerDown(PointerEventData ped)
    {
        Vector2 click;

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(ok.rectTransform,
                                                           ped.position,
                                                           ped.pressEventCamera,
                                                           out click))
        {
            if(index>0) Application.LoadLevel(index);
        }
    }

        public void SetLevel(int i)
        {
            index=i+1;
        }

        public void SetStatusLevel(int lvlNumber, bool isUnlocked, char lvlType, float fixedparameter, float beatenparameter)
        {
            string type = (lvlType=='t')? "Time": "Stars";
            string unlockedString = isUnlocked ? "Yes" : "No";

        transform.Find("STATUS_LVL").transform.GetComponent<Text>().text =
                string.Format(" Level: #{0} \n Unlocked: {1} \n Type: {2} \n Maximum: {3} \n Highscore:{4}",
                                lvlNumber,
                                unlockedString,
                                type,
                                fixedparameter,
                                beatenparameter                                
                             );
        }

}
