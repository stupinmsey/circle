﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SpecialEffectEmitter : MonoBehaviour
{

    public bool ChangeColor;
    public bool ChangeSize;

    [Range(0.1F, 1)]
    public float TimeToChangeColor;
    [Range(0.1F,1)]
    public float TimeToChangeScale;

    private Color aColor, bColor;
    private float colorTimer;

    private Vector2 aScale, bScale;
    private float scaleTimer;

    private BoxCollider2D collider;


    public enum mode
    {
        Cos,
        Parabola
    }

    public mode Mode;

    [Range(0.8F,3.5F)]
    public float ScaleFactor;

    void Start()
    {
        if (ChangeColor)
        {
            if (TimeToChangeColor <= 0) TimeToChangeColor = 1;

            aColor = new Color(Random.Range(0.4f, 1), Random.Range(0.4f, 1), Random.Range(0.4f, 1));
            bColor = new Color(Random.Range(0.4f, 1), Random.Range(0.4f, 1), Random.Range(0.4f, 1));
        }
        if(ChangeSize)
        {
            collider = transform.GetComponent<BoxCollider2D>();
            if (TimeToChangeScale <= 0) TimeToChangeScale = 1;
            if (ScaleFactor <= 0) ScaleFactor = 2;

            aScale = transform.localScale;
            
        }
    }

    void Update()
    {
        if (ChangeColor)
        {
            transform.GetComponent<SpriteRenderer>().material.color = Color.Lerp(aColor, bColor, colorTimer / TimeToChangeColor);
            colorTimer += Time.deltaTime;
            if (colorTimer >= TimeToChangeColor)
            {
                colorTimer = 0;
                aColor = bColor;
                bColor = new Color(Random.Range(0.2f, 1), Random.Range(0.2f, 1), Random.Range(0.2f, 1));
            }
        }

        if (ChangeSize)
        {
            bScale = aScale * ScaleFactor;
            switch (Mode)
            {
                case mode.Cos:
                    {
                        
                        transform.localScale = Vector2.Lerp(aScale, bScale, Mathf.Abs(Mathf.Cos(scaleTimer)));
                        scaleTimer += Time.deltaTime + Mathf.Abs(1-TimeToChangeScale);
                    }
                    break;

                case mode.Parabola:
                    {
                        transform.localScale = aScale * ParabolaTime(scaleTimer / TimeToChangeScale);
                        if (scaleTimer >= TimeToChangeScale) scaleTimer = 0;
                        scaleTimer += Time.deltaTime;
                    }
                    break;
            }

        }
    }

    private float ParabolaTime(float x, bool limited = false)
    {
        float parabolic = (4 * x - 4 * x * x) + 0.3f;
        if (limited) return parabolic < 1 ? parabolic : 1;
        return parabolic;
    }
}
