﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    private Transform player;
    private Vector2 moveDirection;
    private Rigidbody2D rb;

    private bool isDirect;
    private float speed;

    private float killTimer;

	public void OnInit(string command, Vector2 direction, float speed) 
    {
        player = GameObject.Find("Player").transform;
        switch (command)
        {
            case "direct":
                {                    
                    rb = transform.GetComponent<Rigidbody2D>();
                    rb.isKinematic = true;
                    moveDirection = (direction - (Vector2)transform.position).normalized;
                    this.speed = speed;
                    isDirect = true;
                }
                break;
        }
	}
	


	void Update ()
    {
        {
            killTimer += Time.deltaTime;
            if (killTimer > 6) Destroy(this.gameObject);
        }

        if (isDirect)
        {
            rb.MovePosition(Vector3.MoveTowards(transform.position, (Vector2)transform.position + moveDirection, Time.deltaTime * speed));
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log(col.name);
        if (!col.isTrigger)
        {
            Destroy(this.gameObject);
        }
    }
}
