﻿using System;
using UnityEditor;
using UnityEngine;

public class EventsAdvanced : MonoBehaviour
{
    [System.Serializable]
    [HideInInspector]
    public struct Delay
    {
        public float timer;
        public bool inactive;
    }

    public Transform[] units;
    [HideInInspector]
    public bool ShowGizmos;
    [HideInInspector]
    public Vector2[] startPositions;
    [HideInInspector]
    public Vector2[] endPositions;
    [HideInInspector]
    public Delay[] Delays;
    [HideInInspector]
    public float Force;


    public enum What_To_Do { Select, Create, Delete, MotorSwitch }
    public What_To_Do component;

    public bool Trigger;
    private bool Activated;
    //================================

    [HideInInspector]
    public bool CreateTrigger;
    [HideInInspector]
    public bool DeleteTrigger;
    [HideInInspector]
    public bool MotorTrigger;

    //================================


    public void InitializeComponents()
    {
        startPositions = new Vector2[units.Length];
        endPositions = new Vector2[units.Length];
        Delays = new Delay[units.Length];

        component = What_To_Do.Select;

        for (int i = 0; i <= startPositions.Length - 1; i++)
        {
            startPositions[i] = new Vector2(transform.position.x + i - startPositions.Length / 2, transform.position.y - 1);
            endPositions[i] = new Vector2(transform.position.x + i - startPositions.Length / 2, transform.position.y + 1);
        }
    }

    private void Start()
    {
        if (!Trigger) Activated = true;
    }

    private void Update()
    {
        if (Activated)
        {
            for (int i = 0; i <= units.Length - 1; i++)
            {
                if (!Delays[i].inactive)
                {

                    Delays[i].timer -= Time.deltaTime;
                    if (Delays[i].timer <= 0)
                    {
                        Delays[i].inactive = true;
                        if (CreateTrigger)
                        {
                            CreateObject(i);
                        }
                        else if (DeleteTrigger)
                        {
                            DeleteObject(i);
                        }
                        else if (MotorTrigger)
                        {
                            MotorSwitch(i);
                        }
                    }
                }
            }

        }
    }

    private void MotorSwitch(int index)
    {
        WheelJoint2D joint = units[index].gameObject.GetComponent<WheelJoint2D>();
        joint.useMotor = !joint.useMotor;
    }

    private void CreateObject(int index)
    {
        Transform newObj = Instantiate(units[index], startPositions[index], Quaternion.identity);
        var rigidbody2d = newObj.gameObject.GetComponent<Rigidbody2D>() ??
                          newObj.gameObject.AddComponent<Rigidbody2D>();
        rigidbody2d.AddForce((endPositions[index] - startPositions[index]) * Force, ForceMode2D.Impulse);
    }

    private void DeleteObject(int index)
    {
        Destroy(units[index]);
    }

    private void OnTriggerEnter2D()
    {
        if(Trigger) Activated = true;
    }


    void OnDrawGizmos()
    {
        if (ShowGizmos)
        {
            for (int i = 0; i <= startPositions.Length - 1; i++)
            {
                DrawArrow.ForGizmo(startPositions[i], endPositions[i] - startPositions[i], Color.green, 0.3f, 30, 0.75f);
            }
        }
    }



    [CustomEditor(typeof(EventsAdvanced))]
    class TestCustomize : Editor
    {
        bool TimerOptions;
        bool EnableHandles;

        //=========================
        GUIStyle handleStartStyle = new GUIStyle();
        GUIStyle handleEndStyle = new GUIStyle();
        //=========================
        EventsAdvanced events;

        void OnSceneGUI()
        {
            events = (EventsAdvanced)target;
            // Handles.SetCamera(Camera.main);
            if (EnableHandles) DrawNodes(events);
        }

        void OnEnable()
        {
            handleStartStyle.alignment = handleEndStyle.alignment = TextAnchor.MiddleCenter;
            handleStartStyle.fixedWidth = handleEndStyle.fixedWidth = 15;
            handleStartStyle.fixedHeight = handleEndStyle.fixedHeight = 15;
            handleEndStyle.normal.textColor = Color.cyan;
        }


        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Reset setting"))
            {
                events = (EventsAdvanced)target;
                events.InitializeComponents();
                EnableHandles = false;
                TimerOptions = false;
                events.ShowGizmos = false;
            }

            //=========================
            GUIStyle SettingsTopPanel = EditorStyles.objectFieldThumb;
            GUIContent content = new GUIContent(string.Empty);
            //=========================
            try
            {
                switch (events.component)
                {
                    case What_To_Do.Create:
                        {
                            content = new GUIContent("Create Settings");
                            EnableHandles = true;
                            TimerOptions = true;
                            events.ShowGizmos = true;
                            events.DeleteTrigger = false;
                            events.MotorTrigger = false;
                        }
                        break;

                    case What_To_Do.Delete:
                        {
                            content = new GUIContent("Delete Settings");
                            EnableHandles = false;
                            TimerOptions = true;
                            events.ShowGizmos = false;
                            events.DeleteTrigger = true;
                            events.MotorTrigger = false;
                        }
                        break;

                    case What_To_Do.MotorSwitch:
                        {
                            content = new GUIContent("MotorSwitch Settings");
                            EnableHandles = false;
                            TimerOptions = true;
                            events.ShowGizmos = false;
                            events.DeleteTrigger = false;
                            events.MotorTrigger = true;
                        }
                        break;

                        break;
                }
                GUILayout.Label(content, SettingsTopPanel);

                bool _createOptions = EditorGUILayout.BeginToggleGroup("Pause Timers", TimerOptions); //Выключаемая группа элементов

                for (int i = 0; i <= events.Delays.Length - 1; i++)
                {
                    events.Delays[i].timer = EditorGUILayout.Slider("Timer "+i,events.Delays[i].timer, 0.0F, 2.0F);
                }

                events.Force = EditorGUILayout.Slider("Force: ", events.Force, 0.0F, 5.0F);
                EditorGUILayout.EndToggleGroup(); //Конец группы элементов
            }
            catch (System.Exception e)
            {
            }
        }



        void HandleFuncStartPoint(int controlID, Vector3 position, Quaternion rotation, float size)
        {
            if (controlID == GUIUtility.hotControl)
                GUI.color = Color.red;
            else
                GUI.color = Color.green;
            Handles.Label(position, new GUIContent(Resources.Load<Texture>("DotHandle")), handleStartStyle);
            Handles.Label(position, new GUIContent(FindNodeIndex(events.startPositions, position).ToString()), handleStartStyle);
            GUI.color = Color.white;
        }



        void HandleFuncEndPoint(int controlID, Vector3 position, Quaternion rotation, float size)
        {
            if (controlID == GUIUtility.hotControl)
                GUI.color = Color.red;
            else
                GUI.color = Color.grey;
            int indexDir = FindNodeIndex(events.endPositions, position);
            Handles.Label(position, new GUIContent(Resources.Load<Texture>("DotHandle")), handleEndStyle);
            GUI.color = Color.white;
            float dist = Vector2.Distance(events.startPositions[indexDir], position);
            Handles.Label(position, new GUIContent(string.Format("{0:f}", dist)), handleEndStyle);
        }


        private void DrawNodes(EventsAdvanced evnts)
        {
            for (int i = 0; i <= evnts.startPositions.Length - 1; i++)
            {
                Vector3 pos = evnts.startPositions[i];
                float handleSize = HandleUtility.GetHandleSize(pos);
                Vector3 newPos = Handles.FreeMoveHandle(pos, Quaternion.identity, handleSize * 0.09f, Vector3.one, HandleFuncStartPoint);
                if (newPos != pos)
                {
                    evnts.startPositions[i] = newPos;
                }

                pos = evnts.endPositions[i];
                handleSize = HandleUtility.GetHandleSize(pos);
                newPos = Handles.FreeMoveHandle(pos, Quaternion.identity, handleSize * 0.09f, Vector3.one, HandleFuncEndPoint);
                if (newPos != pos)
                {
                    evnts.endPositions[i] = newPos;
                }
            }
        }

        private int FindNodeIndex(Vector2[] worldNodesPositions, Vector2 newNode)
        {
            float smallestdist = float.MaxValue;
            int currentIndex = 0;
            for (int i = 0; i < worldNodesPositions.Length; i++)
            {
                float distance = Vector2.Distance(worldNodesPositions[i], newNode);
                if (distance < smallestdist)
                {
                    currentIndex = i;
                    smallestdist = distance;
                }
            }
            return currentIndex;
        }
    }
}

