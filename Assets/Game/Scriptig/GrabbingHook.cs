﻿using UnityEngine;

public class GrabbingHook : MonoBehaviour {

    public Transform To;
    public Color HookColor;

    private Vector2 p1, p2;
    private float dist;
    private float timer;
    private HookStack hook;
    private bool pushing;

	private void Start() 
    {
        p1 = new Vector2(transform.position.x, transform.position.y);
        p2 = new Vector2(To.transform.position.x, To.transform.position.y);
        dist = Mathf.Sqrt(Mathf.Abs(p2.x - p1.x * p2.x - p1.x + p2.y - p1.y * p2.y - p1.y));
        hook = new HookStack();
        hook.HookInit(p1, p2, HookColor);
        pushing = true;
	}

    private void Update()
    {        
        timer += Time.deltaTime;
        if (timer >= 0.04)
        {
            timer = 0;
            if (pushing) hook.push(ref pushing);
            else hook.pull(ref pushing);
        }
	}


    sealed class HookStack
    {
        float step = 1.1f;
        private Vector2 p1, p2;
        private const int   STACKMAX  = 25;
        GameObject[]        triangles = new GameObject[STACKMAX+1];
        private int         stack     =  0;
        private bool grab, isPaused = false;

        public void HookInit(Vector2 p1, Vector2 p2, Color HookColor)
        {
            this.p1 = p1;
            this.p2 = p2;

            for (byte i = 0; i <= triangles.Length - 1; i++)
            {
                triangles[i] = (GameObject)Instantiate(Resources.Load("TriangleHook"));
                triangles[i].GetComponent<SpriteRenderer>().color = HookColor;
                tempComponent=triangles[i].AddComponent<ActivatingTrigger>();
                tempComponent.isForPlayer = tempComponent.isOnCollisionHook = true;
                tempComponent.enabled = false;
                hide(triangles[i]);
            }
        }


        public void push(ref bool pushing)
        {
            if (!isPaused)
            {

                triangles[stack].transform.position = Vector2.MoveTowards(p1, p2, step * stack);
                triangles[stack].transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(
                                                                      p2.x - triangles[stack].transform.position.x,
                                                                      p2.y - triangles[stack].transform.position.y
                                                                                        ) * -Mathf.Rad2Deg);
                triangles[stack].SetActive(true);
                triangles[stack].GetComponent<ActivatingTrigger>().enabled = true;
                if (stack - 1 >= 0)
                {
                    if (triangles[stack - 1].GetComponent<ActivatingTrigger>().isHooked)
                    {
                        player = triangles[stack - 1].GetComponent<ActivatingTrigger>().GetHookedBody();
                        triangles[stack - 1].GetComponent<ActivatingTrigger>().ClearHookedBody();
                        triangles[stack - 1].GetComponent<ActivatingTrigger>().isHooked =
                        triangles[stack - 1].GetComponent<ActivatingTrigger>().enabled = false;
                        pushing = false;
                        grab = true;
                        return;
                    }
                    triangles[stack - 1].GetComponent<ActivatingTrigger>().enabled = false;
                }
                if (Mathf.Abs(p2.x - triangles[stack].transform.position.x) + Mathf.Abs(p2.y - triangles[stack].transform.position.y) < 2 * step
                   || stack == STACKMAX)
                {
                    foreach (GameObject triangle in triangles)
                    {
                        tempComponent = triangle.GetComponent<ActivatingTrigger>();
                        tempComponent.enabled = true;
                        tempComponent.ClearHookedBody();
                        tempComponent.isHooked = tempComponent.enabled = false;
                    }
                    pushing = false;
                    return;
                }
                stack++;
            }
        }

        public void pull(ref bool pushing)
        {
            if (!isPaused)
            {
                foreach (GameObject triangle in triangles)
                {
                    tempComponent = triangle.GetComponent<ActivatingTrigger>();
                    tempComponent.enabled = true;
                    tempComponent.ClearHookedBody();
                    tempComponent.isHooked = tempComponent.enabled = false;
                }

                if (grab)
                {
                    if (stack == 0)
                    {
                        tempComponent = triangles[stack].GetComponent<ActivatingTrigger>();
                        tempComponent.enabled = true;
                        tempComponent._Camera = GameObject.Find("Main Camera").transform;
                        tempComponent.Die(player);
                        grab = false;
                        isPaused = true;
                        return;
                    }
                    else
                    {
                        player.GetComponent<Rigidbody2D>().velocity *= 0;
                        player.transform.position = triangles[stack].transform.position;
                    }
                }
                triangles[stack].GetComponent<ActivatingTrigger>().enabled = false;
                if (stack - 1 >= 0) triangles[stack - 1].GetComponent<ActivatingTrigger>().enabled = true;
                hide(triangles[stack--]);
                if (stack < 0)
                {
                    pushing = true;
                    stack = 0;
                }
            }
        }

        private void hide(GameObject obj)
        {
            obj.transform.position = faraway;
            obj.SetActive(false);
        }

        private readonly Vector2 faraway = new Vector2(-500, -500);
        private Collider2D player;
        private ActivatingTrigger tempComponent;
    }
}
