﻿using UnityEngine;
using System.Collections;

public class JumpingFigures : MonoBehaviour {


    Vector2 initialScale;
    Rigidbody2D rb;
    RaycastHit2D[] hit;
    float _timer;
    Vector2 to, direction;

    private const float leftPower  =  1300, 
                        rightPower = -1300;

    private int accessL, accessR;

    [Range(0,1.5f)]
    public float _firstDelay;
    public bool Stretch, SideJump;

	
	void Start () {
        initialScale = transform.localScale;
        rb = transform.GetComponent<Rigidbody2D>();
        rb.interpolation = RigidbodyInterpolation2D.Interpolate;
        _timer = 0;
	}
	
	void Update () {




        if (_firstDelay >= 0)
        {
            _firstDelay -= Time.deltaTime;
        }
        else
        {
            _timer += Time.deltaTime;
            if (Stretch && Time.timeScale>0) transform.localScale = new Vector2(transform.localScale.x + 0.01f, transform.localScale.y);
            if (_timer >= 0.75)
            {
                direction = (new Vector2(transform.position.x + 1, transform.position.y) - (Vector2)transform.position).normalized;

                hit = Physics2D.RaycastAll(transform.position, direction, 3);
                foreach (var body in hit)
                {
                    if (body.collider.name != transform.GetComponent<Collider2D>().name) accessR = 0;
                    else accessR = 1;
                }

                direction = (new Vector2(transform.position.x -1, transform.position.y) - (Vector2)transform.position).normalized;

                hit = Physics2D.RaycastAll(transform.position, direction, 3);
                foreach (var body in hit)
                {
                    if (body.collider.name != transform.GetComponent<Collider2D>().name) accessL = 0;
                    else accessL = 1;
                }

                rb.AddForce(Vector2.up * 900, ForceMode2D.Impulse);
                transform.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0.75f, 1), 
                                                                           Random.Range(0.5f, 1), 
                                                                           Random.Range(0.25f, 1), 1);
                if (Stretch) transform.localScale = initialScale;
                if (SideJump) rb.AddForce(Vector2.left * Random.Range(leftPower*accessL, rightPower*accessR), ForceMode2D.Impulse);
                _timer = 0;

            }
        }


	}
}
