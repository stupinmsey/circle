﻿using UnityEngine;
using System.Collections;

public class GameEvents : MonoBehaviour {


    Transform Pluto, Player;
    DialogSystem dialog;
    public bool Once;
    public bool[] Used = new bool[50];
    public bool[] Finished = new bool[50];
    public float[] Timer = new float[50];

	void Start () {
        for (byte i = 0; i < 50; i++)
        {
            if (GameObject.Find(string.Format("Event_{0}", i)) != null)
            {
                Used[i] = (GameObject.Find(string.Format("Event_{0}", i)).GetComponent<GameEvents>().Used[i] == true) ? true : false;
                Finished[i] = (GameObject.Find(string.Format("Event_{0}", i)).GetComponent<GameEvents>().Finished[i] == true) ? true : false;
            }
        }
	}

    private void OnTriggerEnter2D (Collider2D who)
    {
        if (Once && !Used[1] && transform.name == "Event_1" )
        {

            if (who.name == "Player")
            {
                Debug.Log("incoming");
                dialog = transform.GetComponent<DialogSystem>();
                StartCoroutine(TimedDialog(1, "Приветствую тебя, странник. Как ты попал на эту планету???", 6, GameObject.Find("Pluto").transform));
                StartCoroutine(TimedDialog(8, "Попал через мусорку из Расии. Теперь хочу вернуться домой, но не знаю как.", 4, GameObject.Find("Player").transform));
                StartCoroutine(TimedDialog(13, "В таком случае прошу следовать за мной...", 2, GameObject.Find("Pluto").transform));
                Used[1]=true;
            }
        }
    }





    void OnTriggerStay2D(Collider2D who)
    {
        if (Once && !Used[2] && transform.name == "Event_2" &&  GameObject.Find("Event_1").GetComponent<GameEvents>().Finished[1] && who.name == "Player")
        {
                StartCoroutine(TimedMoveAI(1, GameObject.Find("Event_3").transform.position, GameObject.Find("Pluto").transform));
                Used[2] = true;
                Debug.Log("EVENT2");

        }
    }









    private IEnumerator TimedMoveAI(float time1, Vector2 to, Transform t)
    {
        yield return new WaitForSeconds(time1);


    }



    private IEnumerator TimedDialog(float time1, string s, float time2, Transform t)
    {
        yield return new WaitForSeconds(time1);
        dialog.Show(s, time2, t);        
    }

    void Update()
    {


        if (Used[1] && !Finished[1])
        {            
            Timer[1] += Time.deltaTime;
            if (Timer[1] >= 13.5)
            {
                Finished[1] = true;
                Debug.Log("Finished");
            }
        }
    }
}



