﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class BitmapMask : MonoBehaviour
{
    public Texture2D spriteTexture;
    public Texture2D maskedTexture;
    public int diameter = 50;

    public List<bool> mask;

    private float _width;
    private float _height;
    private Sprite _image;
    private SpriteRenderer _spriteRenderer;

    private Color[] tempColors;
    private Color[] maskColors;

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        maskColors = maskedTexture.GetPixels();

        _image = GetMaskedSprite(spriteTexture, maskedTexture);

        _spriteRenderer.sprite = _image;

        _width = _image.bounds.size.x * _image.pixelsPerUnit;
        _height = _image.bounds.size.y * _image.pixelsPerUnit;
    }

    void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        mousePos += (Vector2)_spriteRenderer.sprite.bounds.extents;
        mousePos *= _spriteRenderer.sprite.pixelsPerUnit;

        if (Input.GetMouseButton(0))
        {
            ChangePixels((int)mousePos.x, (int)mousePos.y, diameter, Color.black);
        }

        if (Input.GetMouseButtonDown(1))
        {
            ChangePixels((int)mousePos.x, (int)mousePos.y, 50, Color.white);

        }
    }


    Sprite GetMaskedSprite(Texture2D spriteTexture, Texture2D maskTexture)
    {
        Texture2D texture = new Texture2D(spriteTexture.width, spriteTexture.height);
        Color[] spriteColors = spriteTexture.GetPixels();

        tempColors = new Color[spriteTexture.width * spriteTexture.height];

        for (int y = 0; y < maskTexture.height; y++)
        {
            for (int x = 0; x < maskTexture.width; x++)
            {
                bool masked = (maskColors[x + y * maskTexture.width]) == Color.white; // change to white and change actual image

                if (masked)
                {
                    tempColors[x + y * maskTexture.width] = new Color(0, 0, 0, 0);
                }
                else
                {
                    tempColors[x + y * maskTexture.width] = spriteColors[x + y * maskTexture.width];
                }
            }
        }
        texture.SetPixels(tempColors);
        texture.Apply();

        return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
    }

    void ChangePixels(int centreX, int centreY, int diameter, Color color)
    {
        int radius = diameter / 2;
        int a;
        int b;

        for (int y = centreY - (diameter / 2); y < centreY + (diameter / 2); y++)
        {
            for (int x = centreX - (diameter / 2); x < centreX + (diameter / 2); x++)
            {
                if (x < 0 || y < 0 || x >= _width || y >= _height) continue;

                a = x - centreX;
                b = y - centreY;

                if ((a * a) + (b * b) <= (radius * radius))
                    maskColors[x + y * maskedTexture.width] = color;
            }
        }
        ChangeSprite(GetMaskedSprite(spriteTexture, maskedTexture));
    }

    void ChangeSprite(Sprite updatedSprite)
    {
        _spriteRenderer.sprite = updatedSprite;
    }

}