﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ActivatingTrigger : MonoBehaviour
{
    
    public bool isForPlayer;
    public bool isStar;
    public bool isKillingTrigger;
    public bool isCheckPointTrigger;
    public bool isFinishTrigger;
    public bool isClockStartTrigger;
    public bool isClockEndTrigger;
    public bool isOnCollisionHook;
    public bool isOnCollisionBomb;
    public bool isWaterElement;
    public bool isWind;
    public bool isRelocation;
    public float windPower;
    public Transform RelocationArea;
    private Collider2D relocation;

    private GameObject[] peaces;
    private Vector2 faraway = new Vector2(-300, -300);

    [HideInInspector]
    public Transform _Camera;
    [HideInInspector]
    public bool isHooked = false;

    private bool exploded = false;


    
    public void Die(Transform who)
    {
        Die(who.GetComponent<Collider2D>());
    }

    public void Die(Collision2D who)
    {
        Die(who.collider.GetComponent<Collider2D>());
    }

    public void Die(Collider2D who)
    {
        who.GetComponent<PlayerProperties>().Dead = true;
            GameObject.Find("Data.bin").GetComponent<Preload>().PauseTimer();
            Vector2 temp =who.transform.position;
            _Camera.GetComponent<CameraFollow>().enabled = false;
            who.transform.position = faraway;
            GameObject.Find("Data.bin").GetComponent<Preload>().CreatePieces(temp);
            StartCoroutine(RespawnPlayer(1.5f));
    }

    private IEnumerator RespawnPlayer(float time)
    {
        yield return new WaitForSeconds(time);
        GameObject.Find("Data.bin").GetComponent<Preload>().stars = 0;
        if (GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].lvlType == 't')
        GameObject.Find("Data.bin").GetComponent<Preload>().EndTimer();
        //if (true) ; // сделать проверку на чекпоинты, и исходя из этого убирать кусочки в faraway и телепортировать мячик к чекпоинту

        //GameObject.Find("Data.bin").GetComponent<Preload>().HidePieces();
        Application.LoadLevel(Application.loadedLevel);
    }




    private void OnTriggerEnter2D(Collider2D who)
    {
        if (isRelocation)
        {
            relocation = RelocationArea.GetComponent<Collider2D>();
            float xmin, xmax, ymin, ymax;
            xmax = relocation.bounds.max.x;
            xmin = relocation.bounds.min.x;
            ymax = relocation.bounds.max.y;
            ymin = relocation.bounds.min.y;
            who.transform.position = new Vector2(Random.Range(xmin, xmax), Random.Range(ymin, ymax));
        }

        if (isForPlayer && who.name == "Player")
        {
            if (isKillingTrigger && !who.GetComponent<PlayerProperties>().Dead) Die(who);
            else if (isStar) { GameObject.Find("Data.bin").GetComponent<Preload>().stars++; Destroy(transform.gameObject); }
                if (isFinishTrigger)
                {
                    
                    if (Application.loadedLevel >= Application.levelCount - 1) // проверка на последний уровень
                    {
                        Debug.Log("!!!!!! MAX LEVEL");
                        return;
                    }
                    if (GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].lvlType == 's')
                    {
                        GameObject.Find("Data.bin").GetComponent<SaveLoad>().Save(Application.loadedLevel - 1, 2, GameObject.Find("Data.bin").GetComponent<Preload>().stars);
                        GameObject.Find("Data.bin").GetComponent<SaveLoad>().Save(Application.loadedLevel - 1, 3, GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].beatenparameter);
                    }
                    else if (GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].lvlType == 't')
                    {
                        GameObject.Find("Data.bin").GetComponent<Preload>().PauseTimer();
                        GameObject.Find("Data.bin").GetComponent<SaveLoad>().Save(Application.loadedLevel - 1, 2, GameObject.Find("Data.bin").GetComponent<Preload>().timer);                        
                        GameObject.Find("Data.bin").GetComponent<SaveLoad>().Save(Application.loadedLevel - 1, 3, GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].beatenparameter);
                        
                    }
                    if (GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].lvlType == 's') GameObject.Find("Data.bin").GetComponent<Preload>().stars = 0;
                    else GameObject.Find("Data.bin").GetComponent<Preload>().EndTimer();
                    if (GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].score >= 1 &&
                        !GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel].isUnlocked)
                        GameObject.Find("Data.bin").GetComponent<SaveLoad>().Save(Application.loadedLevel, 1, 1);
                    Application.LoadLevel(1);
                }                
                else if (isClockStartTrigger)
                {
                    GameObject.Find("Data.bin").GetComponent<Preload>().StartTimer(GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].fixedparameter);
                    //Debug.Log(GameObject.Find("Data.bin").GetComponent<SaveLoad>().level[Application.loadedLevel - 1].fixedparameter);
                    
                    Destroy(transform.gameObject);
                }
                

        }


    }

    private Collider2D body;

    private void OnCollisionEnter2D(Collision2D who)
    {
        if (isForPlayer)
        {
            if (isOnCollisionHook && who.gameObject.name == "Player")
            {
                body=who.collider;
                isHooked = true;
            }
            else if (isOnCollisionBomb && who.gameObject.GetComponent<Rigidbody2D>())
            {
                if ((who.relativeVelocity.magnitude>=2.5))
                {
                   ExplodeBomb();
                }         
            }
        }
    }

    public void ExplodeBomb()
    {
        if (!exploded)
        {
            exploded = true;
            GameObject ExplosiveDummy = new GameObject("explosion_dummy");
            ExplosiveDummy.transform.position = transform.position;
            transform.position = faraway;
            var col = ExplosiveDummy.AddComponent<CircleCollider2D>();
            col.usedByEffector = true;
            col.radius = 5f;
            col.isTrigger = true;
            ExplosiveDummy.AddComponent<PointEffector2D>().forceMagnitude = 300;
            if (!GameObject.Find("Player").GetComponent<PlayerProperties>().Dead &&
                Vector2.Distance(GameObject.Find("Player").transform.position, ExplosiveDummy.transform.position) <= 4)
                Die(GameObject.Find("Player").transform);
            StartCoroutine(RemoveObject(ExplosiveDummy, 0.1f));
        }
       
    }
 

    

    private IEnumerator RemoveObject(GameObject dummy, float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(dummy);
    }

    public Collider2D GetHookedBody()
    {
        return body;
    }

    public void ClearHookedBody()
    {
        body = null;
    }


  

    void OnTriggerStay2D(Collider2D col)
    {        
        if (isWaterElement)
        {
            if (this.transform.parent.GetComponent<WaterLine>() &&
                col.GetComponent<Rigidbody2D>())
            {
                if (col.GetComponent<Rigidbody2D>().velocity.magnitude > 5.25 && transform.position.y- 1.1f <col.transform.position.y)
                {                    
                    transform.parent.GetComponent<WaterLine>().Splash(col.transform.position, col.GetComponent<Rigidbody2D>().velocity.magnitude/8f);
                }
               // print(transform.position.y*0.7f + " " +col.transform.position.y);
                
            }
        }
        else if (isWind &&  col.GetComponent<Rigidbody2D>())
        {
            col.GetComponent<Rigidbody2D>().AddForce(transform.right * windPower, ForceMode2D.Impulse);
        }
    }
}
